<?php

use yii\db\Schema;
use yii\db\Migration;

class m150328_160838_create_rooms_table extends Migration
{
	public function safeUp()
    {
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{%rooms}}', [
    			'id_room' => Schema::TYPE_PK,
    			'number' => Schema::TYPE_STRING . ' NOT NULL',
    			'status' => Schema::TYPE_SMALLINT,
    			], $tableOptions);
    	for ($i = 1; $i < 220; $i++) {
	    	$this->insert('{{%rooms}}', [
	    			'number' => $i,
	    			'status' => 1,
	    			]);
    	}
    }

    public function safeDown()
    {
    	$this->dropTable('{{%rooms}}');
    }
}
