<?php

use yii\db\Schema;
use yii\db\Migration;

class m150304_173542_create_reservation_table extends Migration
{
public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
 
        $this->createTable('{{%rooms}}', [
            'id_room' => Schema::TYPE_PK,
			'number' => Schema::TYPE_STRING . ' NOT NULL',
			'last_cleaning' => Schema::TYPE_DATE,
            'status' => Schema::TYPE_SMALLINT,
        ], $tableOptions);
 
        $this->createTable('{{%reservation}}', [
            'id_reservation' => Schema::TYPE_BIGPK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'anons' => Schema::TYPE_TEXT . ' NOT NULL',
            'content' => Schema::TYPE_TEXT . ' NOT NULL',
            'category_id' => Schema::TYPE_INTEGER,
            'author_id' => Schema::TYPE_INTEGER,
            'publish_status' => "enum('" . Post::STATUS_DRAFT . "','" . Post::STATUS_PUBLISH . "') NOT NULL DEFAULT '" . Post::STATUS_DRAFT . "'",
            'publish_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
        ], $tableOptions);
 
        $this->createIndex('FK_post_author', '{{%post}}', 'author_id');
        $this->addForeignKey(
            'FK_post_author', '{{%post}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE'
        );
 
        $this->createIndex('FK_post_category', '{{%post}}', 'category_id');
        $this->addForeignKey(
            'FK_post_category', '{{%post}}', 'category_id', '{{%category}}', 'id', 'SET NULL', 'CASCADE'
        );
    }
 
    public function safeDown()
    {
        $this->dropTable('{{%rooms}}');
        $this->dropTable('{{%rooms}}');
    }
}
