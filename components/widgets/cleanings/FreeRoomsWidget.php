<?php
namespace app\components\widgets\cleanings;

use yii\base\Widget;
use yii\helpers\Html;
use app\models\Rooms;

class FreeRoomsWidget extends Widget {
	
	public $message;
	public $data;
	
	public $onlyAvailable;
	
	public function init() {
		
		parent::init();

		if($this->message === null) {
			$this->message = 'Rooms';
		} else {
			$this->message = $this->message;
		}
	
	}
	
	public function run() {
		$html = Html::beginTag('div', ['class' => 'widget-rooms-container']);
		$html .= Html::tag('h3', $this->message);
		if ( !empty($this->data) && count($this->data) > 0 ) {
			foreach ($this->data as $key => $value) {
				$html .= Html::tag('button', $value['number'], ['class' => 'btn btn-info']); 
			}
		} else {
			$html .= Html::tag('p', 'Free rooms: ' . 127);
			$html .= Html::tag('p', 'Not available: ' . 8);
			$html .= Html::tag('p', 'Other: ' . 54);
		}
		$html .= Html::endTag('div');
		return $html; //Html::encode($html);
	
	}
}

