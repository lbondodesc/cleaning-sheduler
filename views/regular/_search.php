<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CleaningSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cleaning-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_cleaning') ?>

    <?= $form->field($model, 'date_cleaning') ?>

    <?= $form->field($model, 'id_room') ?>

    <?= $form->field($model, 'id_worker') ?>

    <?= $form->field($model, 'notes') ?>

    <?php // echo $form->field($model, 'clean_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
