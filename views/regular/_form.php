<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rooms;
use app\models\Workers;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cleaning-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'date_cleaning')->widget(
	   		 DatePicker::className(), [
	        // inline too, not bad
	        'inline' => false, 
	        // modify template for custom rendering
	        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
	        'clientOptions' => [
	            'autoclose' => true,
	            'format' => 'yyyy-mm-dd'
	        ]
		]); ?> 
    
    <?= $form->field($model, 'id_room')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Rooms::find()->all(), 'id_room', 'number'),
			'language' => 'en',
			'options' => ['placeholder' => 'Select a room ...'],
			'pluginOptions' => [
			'allowClear' => true
			],
			]);
    ?>

    <?= $form->field($model, 'id_worker')->dropDownList(
		ArrayHelper::map(Workers::find()->all(), 'id_worker', 'name'),
    		['prompt' => 'Select Worker']    		
	) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'clean_status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
