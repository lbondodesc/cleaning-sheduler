<?php
/* @var $this yii\web\View */
use yii\helpers\url;
use app\components\widgets\cleanings\FreeRoomsWidget;
use dosamigos\datepicker\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Html;


$this->title = 'Hotel Cleaning Shedule';
?>
<div class="site-index">
	<div class="col-md-2">
 		<h2>Raports</h2>
 		<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
 		<span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
 		<span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
 		<p>
 			<?= Html::dropDownList('raport-type', '0', ['Current week', 'Per Day', 'Diapason'], ['class' => 'form-control']); ?>
 		</p>
 		<p>
 				<?= DatePicker::widget([
				    'name' => 'raportDataPicker',
 					'id'   => 'raportDataPicker',
				    'value' => date('Y-m-d'),
				    'template' => '{addon}{input}',
				        'clientOptions' => [
				            'autoclose' => true,
				            'format' => 'yyyy-mm-dd',
							'weekStart' => 1
				        ]
				]);?>
 		</p>
 		<p>
 			<a class="btn btn-success" id="btnRaportGenerate" target="_blank" href="<?php echo Url::toRoute('calendar/generate-pdf'); ?>">Raport generate</a>
 		</p>

		<?= FreeRoomsWidget::widget(['message' => 'Title Free Rooms']) ?>

	</div>
	
	<div class="col-md-10"> 
		<h2>Shedule for week</h2>
		<div id="cal-nb-container">	
			<a href="<?php echo Url::toRoute(['site/index', 'cal' => 'prev']); ?>" class="calendar-nav-button" id="prev-week">Last Week</a> 
			|<a href="<?php echo Url::toRoute(['site/index', 'cal' => 'current']); ?>" class="calendar-nav-button" id="curr-week">Today</a>  
			|<a href="<?php echo Url::toRoute(['site/index', 'cal' => 'next']); ?>" class="calendar-nav-button" id="next-week">Next week</a> 		 
		</div>
		
		<div id="calendar-table-container"></div>
	</div>
	
</div>

