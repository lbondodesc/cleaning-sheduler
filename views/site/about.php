<?php
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
$this->title = 'About project';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the service for cleaning company. Using by Hotel.
    </p>
    <p>Generate PDF raport</p>
    <p>Generate PDF raport</p>
    
    <div class="col-md-8">
 		<?= 
			Highcharts::widget([
			   'options' => [
			      'title' => ['text' => 'Rooms'],
			      'xAxis' => [
			         'categories' => ['Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat', 'Sun']
			      ],
			      'yAxis' => [
			         'title' => ['text' => 'Cleanings']
			      ],
			      'series' => [
			         ['name' => 'Worker 1', 'data' => [1, 0, 4, 7, 4, 3, 0]],
			         ['name' => 'Worker 2', 'data' => [5, 7, 3, 1, 10, 0, 2]]
			      ]
			   ]
			]);
		?>
		</div>
    

</div>
