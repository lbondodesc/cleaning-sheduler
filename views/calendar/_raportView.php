<h3>Raport</h3>
<p></p>
<table class="table table-bordered raport-table">
	<tr>	
		<th>#</th>
		<th>Room #</th>
		<th>Type</th>
		<th>Notes</th>
		<th>Clean Date</th>
		<th>Status</th>
	</tr>
	<?php foreach ($data as $key => $item): ?>
		<tr>
			<td><?= $key + 1 ?></td>
			<td><?= $item['number'] ?></td>
			<td><?= $item['type'] ?></td>
			<td><?= $item['notes'] ?></td>
			<td><?= $item['date'] ?></td>
			<td><input type="checkbox"/></td>
		</tr>
	<?php endforeach;?>
</table>