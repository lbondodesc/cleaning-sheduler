<?php

use yii\helpers\Html;
use app\components\widgets\cleanings\FreeRoomsWidget;


/* @var $this yii\web\View */
/* @var $model app\models\Reservation */

//$this->title = 'Create Reservation';
$this->params['breadcrumbs'][] = ['label' => 'Reservations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservation-create col-md-4">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<div class="col-md-8">
	<?= FreeRoomsWidget::widget(['data' =>  $rooms, 'message' => 'Free Rooms']) ?>
</div>
