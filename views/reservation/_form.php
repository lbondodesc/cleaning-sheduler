<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Rooms;
use kartik\select2\Select2;
use app\models\Reservation;

/* @var $this yii\web\View */
/* @var $model app\models\Reservation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reservation-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'id_room')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Rooms::find()->all(), 'id_room', 'number'),
			'language' => 'en',
			'options' => ['placeholder' => 'Select a room ...'],
			'pluginOptions' => [
			'allowClear' => true
			],
			]);
    ?>

    <?= $form->field($model, 'lock_start')->widget(
	   		 DatePicker::className(), [
	        // inline too, not bad
	        'inline' => false, 
	        // modify template for custom rendering
	        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
	        'clientOptions' => [
	            'autoclose' => true,
				'format'	=> Yii::$app->formatter->dateFormat, 
	        ]
		]); ?> 

     <?= $form->field($model, 'lock_finish')->widget(
	   		 DatePicker::className(), [
	        // inline too, not bad
	        'inline' => false, 
	        // modify template for custom rendering
	        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
	        'clientOptions' => [
	            'autoclose' => true,
				'format'	=> Yii::$app->formatter->dateFormat,
	        ]
		]); ?> 
    

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_cancel')->widget(Select2::classname(), [
				'data' => ArrayHelper::map(Reservation::getYesNo(), 'is_cancel', 'title'),
				'language' => 'en',
				'options' => [	'placeholder' => 'Cancel all cleanings ...' ],
			]);
    ?>

    
    
    <?= $form->field($model, 'cleaning_days')->widget(Select2::classname(), [
			'data' => ArrayHelper::map(Reservation::getDaysLabel(), 'cleaning_days', 'title'),
			'language' => 'en',
			'options' => [	'placeholder' => 'Select a Days ...', 
							'multiple' => true
							],
			'pluginOptions' => [
					'allowClear' => true
				],
			]);
    ?>
    
    <?= $form->field($model, 'group_t')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
