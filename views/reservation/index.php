<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Reservation;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReservationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reservations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Reservation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id_reservation',
			['label'=>'Room', 'attribute'=>'roomNumber'],
            'lock_start',
            'lock_finish',
            'notes:ntext',
            ['label'=>'Is Cancel', 'value'=>function ($searchModel, $index, $widget) { return $searchModel->is_cancel == 0 ? 'No' : 'Yes'; }],

			['label'=>'Cleaning Day', 'value'=>function ($searchModel, $index, $widget) { 
												return Reservation::getDaysLabel($searchModel->cleaning_days);	
											}],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
