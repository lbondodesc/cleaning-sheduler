<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\alert\AlertBlock;
use kartik\alert\Alert;
use kartik\grid\EditableColumn;
use yii\helpers\Url;
use app\models\Reservation;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Reservation */

$this->title = 'Reservation and cleanings of ' . $model->roomNumber;
$this->params['breadcrumbs'][] = ['label' => 'Reservations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$flashSession = yii::$app->session->getFlash('regularsAdded');
?>
<?php if(isset($flashSession)): ?>
	<?= 
		 Alert::widget([
			'type' => Alert::TYPE_INFO,
			'title' => 'Session',
			'titleOptions' => ['icon' => 'info-sign'],
			'body' => $flashSession,
			]); 
	?>
<?php endif;?>
<div class="reservation-view">
	<div class="col-md-12">
			    <h1><?= Html::encode($this->title) ?></h1>
	</div>
	
	<div class="col-md-8">
			        <a class="btn btn-success" href="<?php echo Url::toRoute('reservation/index'); ?>">Add cleaning</a>
			        <?= GridView::widget([
        'dataProvider' => $regDataProvider,
        'filterModel' => $regSearhModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
		//	'cleaning_id',
		//	'id_reservation',
			'cleaning_date',
			'notes',
		//	'id_worker',
			'type',
			'is_cancel',
        ],
    ]); ?>
		<ul>
			<li>Remake without aftersave, and add button Generate cleanings with parametr "Per week" and add inregular</li>
			<li>Pdf generate cleanings for this reservation</li>
			<li>Grid can edit add new cleanings without reload page and other pages</li>
			<li>Kartik grid view</li>
			<li>Extend reservation controller</li>
		</ul>
		
	</div>
	<div class="col-md-4">
		
	    <p>
	        <?= Html::a('Update', ['update', 'id' => $model->id_reservation], ['class' => 'btn btn-primary']) ?>
	        <?= Html::a('Delete', ['delete', 'id' => $model->id_reservation], [
	            'class' => 'btn btn-danger',
	            'data' => [
	                'confirm' => 'Are you sure you want to delete this item?',
	                'method' => 'post',
	            ],
	        ]) ?>
	        <a class="btn btn-success" href="<?php echo Url::toRoute('reservation/index'); ?>">Generate cleanings</a>
	    </p>
	
	    <?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [
	            'id_reservation',
	            ['label'=>'Room', 'attribute'=>'roomNumber'],
	            'lock_start',
	            'lock_finish',
	            'notes:ntext',
				[
					'label' => 'Is Cancel',
					'value' => $model->is_cancel == 0 ? 'No' : 'Yes'
				],
	            'cleaning_days',	
	    		[
					'label' => 'Cleaning Days',
					'value' =>	Reservation::getDaysLabel($model->cleaning_days)
			    ],
	        ],
	    ]) ?>
	    
		
	</div>

</div>

<?php

?>

