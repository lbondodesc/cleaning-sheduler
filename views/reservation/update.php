<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reservation */

$this->title = 'Update Reservation: ' . ' ' . $model->id_reservation;
$this->params['breadcrumbs'][] = ['label' => 'Reservations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_reservation, 'url' => ['view', 'id' => $model->id_reservation]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reservation-update">
	<div class="col-md-6">
	    <h1><?= Html::encode($this->title) ?></h1>
	
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
