USE yii_cleaning_sheduler;

/* Select reserved rooms */
/*
SELECT rooms.id_room, rooms.number, reservation.cleaning_day, reservation.lock_start, reservation.lock_finish, reservation.notes 
FROM rooms
RIGHT JOIN reservation ON rooms.id_room = reservation.id_room
WHERE CURDATE() BETWEEN reservation.lock_start AND reservation.lock_finish
*/
/* Select free rooms */
/*
SELECT rooms.id_room, rooms.number 
FROM rooms
LEFT JOIN reservation ON rooms.id_room = reservation.id_room
WHERE rooms.id_room NOT IN (
				SELECT reservation.id_room
				FROM reservation
				WHERE CURDATE() BETWEEN reservation.lock_start AND reservation.lock_finish
			)
*/
/* Select reserved rooms which cancel cleaning */
/*
SELECT rooms.id_room, rooms.number, reservation.cleaning_day, reservation.lock_start, reservation.lock_finish, reservation.notes 
FROM rooms
RIGHT JOIN reservation ON rooms.id_room = reservation.id_room
WHERE reservation.is_cancel = 1 AND CURDATE() BETWEEN reservation.lock_start AND reservation.lock_finish
*/
/* SELECT ALL ACTUAL RESERVATIONS */
/*
SELECT * 
FROM reservation
LEFT JOIN rooms ON rooms.id_room = reservation.id_room
WHERE reservation.lock_finish >= CURDATE()
*/
/* GET MIN ACTUAL RESERVATION DATE */
/*
SELECT MIN(reservation.lock_start)
FROM reservation
WHERE reservation.lock_finish >= CURDATE()
*/
/* GET MAX ACTUAL RESERVATION DATE*/
/*
SELECT MAX(reservation.lock_finish)
FROM reservation
WHERE reservation.lock_finish >= CURDATE()
*/

/* GET DATA FOR CALENDAR */
/*
SELECT rooms.number, reservation.notes AS notes , reservation.lock_start as clean_date
FROM rooms
RIGHT JOIN reservation ON rooms.id_room = reservation.id_room
WHERE reservation.is_cancel = 1 AND CURDATE() BETWEEN reservation.lock_start AND reservation.lock_finish
UNION
SELECT rooms.number, reservation.notes AS notes, reservation.lock_finish AS clean_date
FROM rooms
RIGHT JOIN reservation ON rooms.id_room = reservation.id_room
WHERE reservation.is_cancel = 1 AND CURDATE() BETWEEN reservation.lock_start AND reservation.lock_finish
UNION
SELECT rooms.number, cleaning.notes AS notes, cleaning.date_cleaning AS clean_date
FROM rooms
RIGHT JOIN cleaning ON cleaning.id_room = rooms.id_room
*/
/* GET MAX CLEANING ROOMS ON 1 DAY */
