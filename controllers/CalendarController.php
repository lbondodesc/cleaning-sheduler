<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Request;
use app\models\Calendar;
use app\models\RegularCleanings;
use yii\helpers\Json;
use kartik\mpdf\Pdf;


class CalendarController extends Controller
{
	
	/**
	 * Init for ajax calendar in index.html
	 * Make calendar as widget !!!
	 * @return \yii\web\Response|\yii\console\Response
	 */
	
	public function getData() 
	{
		$calendarModel = new Calendar();
		return $calendarModel->getCalendarData();
	}
    public function actionInit()
    {
    	$request = Yii::$app->request;
    	
    	/* make sort of array by date before sending */
    	$cleanings = $this->getData();
    	/*if ($request->isAjax) { 
    			
    	}*/

    	$response = Yii::$app->response;
    	$response->format = \yii\web\Response::FORMAT_JSON;
    	$response->data = Json::encode($cleanings);
    	return $response;
    }
    
    /**
     * @param int $id_cleanig
     * @param string $newNote
     * @return boolean
     */
    public function actionUpdateNote($id_cleaning, $newNote = '') {
    	$session = Yii::$app->getSession();
    	$message = '';
    	
    	$model = RegularCleanings::findOne($id_cleaning);
    	$model->notes = $newNote;
    	$model->update();
    	
   		if ($this->update() !== false) {
    		$message = 'Note was updated.';
		} else {
    		$message = 'Something wrong! Note was not updated!';
		}
		$session->setFlash('regularsAdded', $message);
    }
    
    /**
     * 
     * @param string $filterDate
     * @param string $type
     * @return \kartik\mpdf\mixed|NULL
     */
    public function actionGeneratePdf($filterDate = '', $type = 'cpday') {
    	
		$calendarModel = new Calendar();
    	
		if( empty($filterDate) ) {
			$filterDate = date('Y-m-d');
		}
    	
    	$data = $this->getData();
    	
    	if ($type === 'cpday') {
    		
    		$content = $this->renderPartial('_raportView', ['data' => $data], false, true);
    		
    		$pdf = new Pdf([
    				'mode' => Pdf::MODE_UTF8,
    				'format' => Pdf::FORMAT_A4,
    				'orientation' => Pdf::ORIENT_PORTRAIT,
    				'destination' => Pdf::DEST_BROWSER, //Pdf::DEST_DOWNLOAD //Pdf::DEST_BROWSER
    				'content' => $content,
    				'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
    				'cssInline' => '.kv-heading-1{font-size:18px} .raport-table tr td {padding: 5px} .raport-table{width: 100%}',
    				'options' => ['title' => 'Hotel Cleaning Shedule'],
    				'methods' => [
		    				'SetHeader'=>['Hotel Cleaning Shedule. Raport'],
		    				'SetFooter'=>['{PAGENO}'],
    					],
    				'filename' => 'raportperday',
    			]);
    		
    		return $pdf->render();

    	} else if ($type === 'cpweek') {
    		return null;
    	}
    
    }

}
