<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "regular_cleanings".
 *
 * @property string $cleaning_id
 * @property string $id_reservation
 * @property string $cleaning_date
 * @property string $notes
 * @property integer $id_worker
 * @property integer $type
 * @property integer $is_cancel
 * @property string $room_num
 *
 * @property Reservation $idReservation
 */
class RegularCleanings extends \yii\db\ActiveRecord
{
	const CLEAN_START = 0;
	const CLEAN_FINISH = 1;
	const CLEAN_REGULAR = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regular_cleanings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_reservation'], 'required'],
            [['id_reservation', 'id_worker', 'type', 'is_cancel'], 'integer'],
            [['cleaning_date'], 'safe'],
            [['notes'], 'string', 'max' => 255],
            [['room_num'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cleaning_id' => 'Cleaning ID',
            'id_reservation' => 'Id Reservation',
            'cleaning_date' => 'Cleaning Date',
            'notes' => 'Notes',
            'id_worker' => 'Id Worker',
            'type' => 'Type',
            'is_cancel' => 'Is Cancel',
            'room_num' => 'Room Num',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdReservation()
    {
        return $this->hasOne(Reservation::className(), ['id_reservation' => 'id_reservation']);
    }
   
    public function getActualCleanings(array $ids = null)
    {
    	 
    	$cleanings = self::find()
    	->where(['in','id_reservation', $ids])
    	->orderBy('cleaning_date')
    	->asArray()
    	->all();
    
    	return $cleanings;
    }
}
