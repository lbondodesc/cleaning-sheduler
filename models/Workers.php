<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "workers".
 *
 * @property integer $id_worker
 * @property string $name
 * @property string $phone
 */
class Workers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'workers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_worker' => 'Id Worker',
            'name' => 'Name',
            'phone' => 'Phone',
        ];
    }
}
