<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RegularCleanings;

/**
 * SearchRegularCleanings represents the model behind the search form about `app\models\RegularCleanings`.
 */
class SearchRegularCleanings extends RegularCleanings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cleaning_id', 'id_reservation', 'id_worker', 'type', 'is_cancel'], 'integer'],
            [['cleaning_date', 'notes', 'room_num'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RegularCleanings::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'cleaning_id' => $this->cleaning_id,
            'id_reservation' => $this->id_reservation,
            'cleaning_date' => $this->cleaning_date,
            'id_worker' => $this->id_worker,
            'type' => $this->type,
            'is_cancel' => $this->is_cancel,
        ]);

        $query->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'room_num', $this->room_num]);

        return $dataProvider;
    }
}
