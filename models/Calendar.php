<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\models\RegularCleanings;
use app\models\Reservation;
use yii\i18n\Formatter;

/**
 * ContactForm is the model behind the contact form.
 */
class Calendar extends Model
{

	private $calendarData = array();
	
	/**
	 * 
	 * @return multitype: array
	 */
	public function getCalendarData()
	{
		$this->formCleaningsList();
		return $this->calendarData;
	}
	
	/**
	 * 
	 */
	private function formCleaningsList()
	{
		$statuses = array( 'start' , 'finish', 'regular');
		$actualResIds = (new Reservation)->getActualReservationIds();
		$modelReg = new RegularCleanings();
		
		$rows = $modelReg->getActualCleanings($actualResIds);

		foreach($rows as $cleaning) {

			$this->calendarData[] = array(
					'number' 		=> 	$cleaning['room_num'],
					'date'	 		=> 	$cleaning['cleaning_date'],
					'notes'			=> 	$cleaning['notes'],
					'cleaning_day'	=> 	'Monday',
					'type'		   	=> 	$statuses[intval($cleaning['type'])],
					'status'		=> 	$cleaning['is_cancel'],
					'worker'		=> 	$cleaning['id_worker'],
					'id_reserv'		=> 	$cleaning['id_reservation'],
					'id'			=>	'reg' . $cleaning['cleaning_id'],
			);
		}
		$list = array();	
	}
}
