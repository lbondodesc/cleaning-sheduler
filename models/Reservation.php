<?php

namespace app\models;

use Yii;
use app\models\RegularCleanings;

/**
 * This is the model class for table "reservation".
 *
 * @property string $id_reservation
 * @property integer $id_room
 * @property string $lock_start
 * @property string $lock_finish
 * @property string $notes
 * @property integer $is_cancel
 * @property integer $cleaning_days
 * @property datetime $created_time
 * @property integer $id_manager
 * @property string $group_t
 */
class Reservation extends \yii\db\ActiveRecord
{
	
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reservation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_room', 'is_cancel', 'id_manager'], 'integer'],
            [['lock_start', 'lock_finish'], 'safe'],
            [['notes', 'created_time', 'cleaning_days', 'group_t'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_reservation' => 'Id Reservation',
            'id_room' => 'Id Room',
            'lock_start' => 'Arrival Date',
            'lock_finish' => 'Departure Date',
            'notes' => 'Notes',
            'is_cancel' => 'Is Cancel',
            'cleaning_days' => 'Cleaning Days',
            'created_time' => 'Created Time',
            'id_manager' => 'Id Manager',
            'group_t' => 'Group'
        ];
    }
    
    
    public function beforeSave($insert)
    {
    	var_dump($insert);
    	var_dump($insert['lock_start']);
    	var_dump($insert['lock_finish']);
    	$insert['lock_start'] = dateFormat($insert['lock_start']);
    	
    }

    /**
     * 
     * @param unknown $date
     * @return DateTime date New formatted date 
     */
    private function dateFormat($date) 
    {
    	return DateTime::createFromFormat(Yii::$app->formatter->dateFormat, $date)->format('d-m-Y');	
    }
    
    /**
     * @param bool  $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

	    parent::afterSave($insert, $changedAttributes);
		
	    $data = array();
	    $session = Yii::$app->getSession();
	    
	    //Get Room number by id room 
	    $idRoom = isset($changedAttributes['id_room']) ? intval($changedAttributes['id_room']) : $this->id_room;
	    $roomNumber = Rooms::findOne($idRoom)->number;
	    
	    //For array data for all regular cleanings records of current reservation 
	    $data['id_reservation'] = $this->id_reservation;
	    $data['notes'] = 'Input note, please!';
	    $data['room_num'] = $roomNumber;
	    $data['is_cancel'] = isset($changedAttributes['is_cancel']) ? $changedAttributes['is_cancel'] : $this->is_cancel;
	    $start = isset($changedAttributes['lock_start']) ? $changedAttributes['lock_start'] : $this->lock_start;
	    $finish = isset($changedAttributes['lock_finish']) ? $changedAttributes['lock_finish'] : $this->lock_finish;
	    
	    //Insert or update reservation, regenerate regular cleanings records
	    if ($insert) {
	    	$amountInserted = $this->generateRegularCleanings($this->lock_start, $this->lock_finish, $data);
	    	$message = 'Can not add regulars cleaning.';
	    	if ($amountInserted) {
	    		$message = 'Regular Cleanings have successfully added. ' . $amountInserted;
	    	}
	    } else if ( isset($changedAttributes['lock_start']) 	 || isset($changedAttributes['lock_finish'])
	    			|| isset($changedAttributes['cleaning_days']) || isset($changedAttributes['id_room'])) {
	    	$amountInserted = $this->generateRegularCleanings($start, $finish, $data);
	    	$message = 'Date have updated! Created regular cleanings - ' . $amountInserted;
	    } else {
	    	$message = 'Updated somethig else! Check it!';
	    }
	    $session->setFlash('regularsAdded', $message);
 
    }
    
    /**
     * @return array $regData
     */
    private function generateRegularCleanings($lockStart, $lockFinish, $data = null) {
    	$regData = array();
    	$dates = $this->getDiffWeeks($lockStart, $lockFinish);

    	$amount = count($dates);
    	if ($dates) {
	    	
	    	for ($i = 0; $i < $amount; $i++) {
	    		$type = ($i === 0 ? RegularCleanings::CLEAN_START :
	    				( ($i === ($amount-1)) ? RegularCleanings::CLEAN_FINISH : (RegularCleanings::CLEAN_REGULAR)));
	    				
	    		$regData[] = [ $data['id_reservation'], $dates[$i], $data['notes'], '1', $type, 0, $data['room_num'] ];
	    	}

	    	$db = $this->getDb();
	    	return $db->createCommand()->batchInsert('regular_cleanings', 
			    			['id_reservation', 'cleaning_date', 'notes', 'id_worker', 'type', 'is_cancel', 'room_num'], 
			    			$regData
    					)->execute();
    	} else {
    		return false;
    	}
    	
    }
    
    /**
     *
     * @return array $weeks
     */
    public function getDiffWeeks($lockStart, $lockFinish, $cleaningDay = null)
    {
    	$diffDates = [];
    	
    	$lockStart = new \DateTime($lockStart);
    	$lockFinish = new \DateTime($lockFinish);
    	$interval = $lockFinish->diff($lockStart)->days;
    	$weeksDiff = floor( $interval / 7 );
   		
    	if ($weeksDiff < 1) {
    		return [$lockStart->format(Yii::$app->formatter->dateFormat), $lockFinish->format(Yii::$app->formatter->dateFormat)];
    	}
    	
    	array_push($diffDates, $lockStart->format(Yii::$app->formatter->dateFormat));
    	$currWeek = $lockStart;
    	for ($i = 0; $i < $weeksDiff; $i++) {
    		$currWeek = $currWeek->add(new \DateInterval('P7D'));
    		array_push($diffDates, $currWeek->format(Yii::$app->formatter->dateFormat));
    	}
    	
    	array_push($diffDates, $lockFinish->format(Yii::$app->formatter->dateFormat));
    	
    	return $diffDates;
    	
    }
    
    public function getRooms() {
    	return $this->hasOne(Rooms::className(), ['id_room' => 'id_room']);
    }
    
    public function getRoomNumber() {
    	return $this->rooms->number;
    }
    
    /**
     * 
     * @return multitype:
     */
    public function getActualReservations()
    {
    	$reservations = Reservation::find()
			    	->where(['>', 'reservation.lock_finish', date(Yii::$app->formatter->dateFormat)])
			    	->orderBy('id_reservation')
			    	->all();
    	return array_values($reservations);
    }
    
    /**
     * 
     * @return multitype:NULL |multitype:
     */
    public function getActualReservationIds()
    {
    	$reservations = $this->getActualReservations();
    	$iDs = array();
    	if ($reservations) {
    		foreach ($reservations as $key => $value) {
    			$iDs[] = intval($value->id_reservation);
    		}
    		return $iDs;
    	} else {
    		return array();
    	}
    	
    }
    /**
     * 
     * @param number $num
     * @return multitype:multitype:number string
     */
    public static function getDaysLabel($num = null) {
    	$days = [
	    			['cleaning_day' => 0, 'title' => 'Monday'],
	    			['cleaning_day' => 1, 'title' => 'Thuesday'],
	    			['cleaning_day' => 2, 'title' => 'Wednesday'],
	    			['cleaning_day' => 3, 'title' => 'Thursday'],
	    			['cleaning_day' => 4, 'title' => 'Friday'],
	    			['cleaning_day' => 5, 'title' => 'Saturday'],
	    			['cleaning_day' => 6, 'title' => 'Sunday']
    		];
    	
    	if (is_null($num)) { return $days; }
    	
    	if (intval($num) >=0 && intval($num <= 6)) {
    		return $days[$num]['title'];
    	}			
    }
    
    public static function getYesNo() {
    	 
    	return [
    		['is_cancel' => 0, 'title' => 'No'],
    		['is_cancel' => 1, 'title' => 'Yes'],
    	
    	];
    }
}
