<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "rooms".
 *
 * @property integer $id_room
 * @property string $number
 * @property string $last_cleaning
 * @property integer $status
 */
class Rooms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rooms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['last_cleaning'], 'safe'],
            [['status'], 'integer'],
            [['number'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_room' => 'Id Room',
            'number' => 'Number',
            'last_cleaning' => 'Last Cleaning',
            'status' => 'Status',
        ];
    }
    
    /**
     * 
     * @return Ambigous <multitype:, \yii\db\mixed, mixed>
     */
    public function getReservedRooms()
    {
    	$rooms = Rooms::find()->joinWith([
		    'reservation' => function ($query) {
		        $query->where(':now BETWEEN reservation.lock_start AND reservation.lock_finish')
		        		->addParams([':now' => date('Y-m-d')])->distinct();
		    }
		])->all();
		return $rooms;
	    
    }
    
    /**
     * @return ActiveRecord object 
     */
    public function getReservation()
    {
    	return $this->hasMany(Reservation::className(), ['id_room' => 'id_room']);
    }
    /*
    SELECT rooms.id_room, rooms.number
    FROM rooms
    LEFT JOIN reservation ON rooms.id_room = reservation.id_room
    WHERE rooms.id_room NOT IN (
    		SELECT reservation.id_room
    		FROM reservation
    		WHERE CURDATE() BETWEEN reservation.lock_start AND reservation.lock_finish
    )
    */
    /**
     * 
     * @return array $rooms
     */
    public function getFreeRooms() {
    	$rdRooms = (new Query)->select('reservation.id_room')
    				->from('reservation')
    				->where(':now BETWEEN reservation.lock_start AND reservation.lock_finish')
		        	->addParams([':now' => date('Y-m-d')]);
    	
		$rooms = self::find()->joinWith([
		    'reservation' => function ($query) use ($rdRooms) {
		        $query->where(['rooms.id_room' => $rdRooms]);
		    }
		])->all();
		return $rooms;
    }
}
