USE yii_cleaning_sheduler;

SELECT rooms.number, reservation.notes AS notes , reservation.lock_start AS clean_date
FROM rooms
RIGHT JOIN reservation ON rooms.id_room = reservation.id_room
WHERE reservation.is_cancel = 0 AND CURDATE() BETWEEN reservation.lock_start AND reservation.lock_finish
UNION
SELECT rooms.number, reservation.notes AS notes, reservation.lock_finish AS clean_date
FROM rooms
RIGHT JOIN reservation ON rooms.id_room = reservation.id_room
WHERE reservation.is_cancel = 0 AND CURDATE() BETWEEN reservation.lock_start AND reservation.lock_finish
UNION
SELECT rooms.number, cleaning.notes AS notes, cleaning.date_cleaning AS clean_date
FROM rooms
RIGHT JOIN cleaning ON cleaning.id_room = rooms.id_room
WHERE cleaning.clean_status = 1
