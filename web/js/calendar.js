;( function(){
	
	var eventsObj = {
		
		addEvent: function(el, type, fn) {
				if (typeof addEventListener !== 'undefined') {
					el.addEventListener(type, fn, false);
				} else if (typeof attachEvent !== 'undefined') {
						el.attachEvent('on' + type, fn);
				} else {
						el['on' + type] = fn;
				}
			},

		preventDefault: function(event) {
				if (typeof event.preventDefault !== 'undefined') {
					event.preventDefault();
				} else {
					event.returnValue = false;
				}
			},
		removeEvent: function(el, type, fn) {
				if (typeof removeEventListener !== 'undefined') {
					el.removeEventListener(type, fn, false);
				} else if (typeof detachEvent !== 'undefined') {
					el.detachEvent('on' + type, fn);
				} else {
						el['on' + type] = null;
				}
			},
		getTarget: function(event) {
				if (typeof event.target !== 'undefined') {
					return event.target;
				} else {
					return event.srcElement;
				}
			},
	}
	
	var getXmlHttp = function() {
		var xmlhttp;
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (E) {
				xmlhttp = false;
			}
		}
		if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
			xmlhttp = new XMLHttpRequest();
		}
		return xmlhttp;
	}
	
	var getBaseUrl = function() {
		var fullUrl = location.toString();
		var endUrl = fullUrl.indexOf('?');
		return fullUrl.substr(0, endUrl);
	}
	
	var calendar = {
			
			DAYS: ['Monday', 'Tuesday', 'Wensday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
			current_week: '4',
			format: 'DD-MM-YYYY',
			
			getNextWeek: function() {
				var m = moment(); 
				var res = moment().add(1, 'weeks');
				return res;
			},
			
			getPrevWeek: function() {
				var m = moment();
				var res = moment().subtract(1, 'weeks');
				return res;
			},
			
			getCurrWeek: function() {
				var m = moment();
				var res = moment();
				return res;
			},
			
			getWeekDates: function(week) {
				
				var weekDates = [],
					i;
				// Check for Moment Object
				var weekStart = week.startOf('isoweek'); 
				
				weekDates.push(weekStart.format(calendar.format));
				
				for (i = 1; i < 7; i++) {
					weekDates.push(weekStart.add(1, 'd').format(calendar.format));
				}
				return weekDates;
			},
			
			newDraw: function(data) {
				var weekTableContainer = document.getElementById('calendar-table-container'),
					table = document.createElement('table'),
					tbody = document.createElement('tbody'),
					captionTr = document.createElement('tr'),
					i,
					j,
					maxPerDay = 0;
				
				table.className = 'table table-bordered';
				table.setAttribute('id', 'calendar-table');

				for (i = 0; i < data.length; i++ ) {
					if (data[i].length > maxPerDay) {
						maxPerDay = data[i].length;
					}
				}

				for (i = 0; i < data.length; i++ ) {
					var tdContent = data[i][0] + ' ' + calendar.DAYS[i];
					var td = document.createElement('th');
					td.innerHTML = tdContent;
					td.setAttribute('id', data[i][0]);
					if (data[i][0] === moment().format(calendar.format)) {
						td.className = 'th-today';
					}
					captionTr.appendChild(td);
				}
				
				tbody.appendChild(captionTr);

				for (j = 0; j < maxPerDay; j++ ) {
					var roomsTr = document.createElement('tr');
					roomsTr.className = 'active';
					for (i = 0; i < data.length; i++) {
						var tdContent = (undefined !== data[i][j+1]) ? data[i][j+1].number : '';
						var td = document.createElement('td');
						td.innerHTML = tdContent;
						if (undefined !== data[i][j+1]) {
							if(data[i][j+1].type === 'regular') {
								td.className = 'info';
							} else if (data[i][j+1].type === 'start') {
								td.className = 'success';
							} else {
								td.className = 'danger';
							}
							td.setAttribute('data-toggle', 'popover');
							td.setAttribute('title', 'Room #' + data[i][j+1].number);
							td.setAttribute('data-content', data[i][j+1].notes);
							td.setAttribute('data-placement', 'top');
							td.setAttribute('tabindex', '0');
							td.setAttribute('data-trigger', 'focus');
						}
						td.setAttribute('id', data[i][0]);
						roomsTr.appendChild(td);
					}
					tbody.appendChild(roomsTr);
				}

				table.appendChild(tbody);
				
				if (weekTableContainer !== null) {
					weekTableContainer.appendChild(table);
				}
				
				$(function () {
					  $('[data-toggle="popover"]').popover({
					    container : 'body'
					  });
				});
			},
	};
		
	var init = function (callback) {
		var req = getXmlHttp(),
			newObj;
		
		req.onreadystatechange = function() {  
			if (req.readyState == 4) { 
				console.log(req.statusText); 
				if(req.status == 200) { 
					var obj = JSON.parse(req.responseText);
					newObj = JSON.parse(obj);
					if (typeof callback === 'function') {
						callback.call(this, newObj);
					}	
				}
			}
		}
		req.open('GET', getBaseUrl() + '?r=calendar/init', true);  
		req.send(null);  	  
		console.log('Waiting for response from server Init action...');
	}
	
	var calendarDataForm = function(data) {
		var weekDates = calendar.getWeekDates(calendar.getCurrWeek()),
			navBContainer = document.getElementById('cal-nb-container'),
			weekTableContainer = document.getElementById('calendar-table-container');
		
		calendar.newDraw(formWeekDates(weekDates, data));
		
		if(navBContainer !== null) {
			eventsObj.addEvent(navBContainer, 'click', function(e) {
				eventsObj.preventDefault(e);
				var elem = eventsObj.getTarget(e);
				
				if (weekTableContainer.hasChildNodes()) {
					console.log(true);
					var oldTable = document.getElementById('calendar-table');
					weekTableContainer.removeChild(oldTable);
				}
	
				switch (elem.id) {
					case 'prev-week':
						weekDates = calendar.getWeekDates(calendar.getPrevWeek());
						break;
					case 'next-week':
						weekDates = calendar.getWeekDates(calendar.getNextWeek());
						break;
					case 'curr-week':
						weekDates = calendar.getWeekDates(calendar.getCurrWeek());
						break;
					default:
						weekDates = calendar.getWeekDates(calendar.getCurrWeek());		
				}
				calendar.newDraw(formWeekDates(weekDates, data));
			
			});
		}
	}
	
	var formWeekDates = function(weekDates, data) {
		console.log(data);
		var result = [],
		i;

		for (i = 0; i < weekDates.length; i++) {
			var temp = [weekDates[i]];
			for (var item in data) {
				if (weekDates[i] === data[item].date) {
					temp.push(data[item]);
				}
			}
			result.push(temp);
		}
		return result;
	}

	init(calendarDataForm);
	/*
	var m = moment();
	var weekStart = m.startOf('isoweek'); 
	var next = moment().add(1, 'weeks').startOf('isoWeek');
	var prev = moment().subtract(1, 'weeks').startOf('isoWeek');
	console.log(prev.format('YYYY-MM-DD'));
	console.log(next.format('YYYY-MM-DD'));
	*/
})();
