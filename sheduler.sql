/*
SQLyog Ultimate v8.32 
MySQL - 5.1.73-community : Database - yii_cleaning_sheduler
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `group` */

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group` (
  `id_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `color` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `group` */

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values ('m000000_000000_base',1427558410),('m150304_173324_create_rooms_table',1427558555),('m150304_173428_create_reservation_table',1427558555),('m150304_173456_create_regular_cleaings_table',1427558555),('m150304_173527_create_cleaning_table',1427558555),('m150304_173542_create_reservation_table',1427558555);

/*Table structure for table `regular_cleanings` */

DROP TABLE IF EXISTS `regular_cleanings`;

CREATE TABLE `regular_cleanings` (
  `cleaning_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `id_reservation` bigint(20) NOT NULL COMMENT 'Foreign key',
  `cleaning_date` date DEFAULT NULL COMMENT 'Form in php model for every record',
  `notes` varchar(255) DEFAULT NULL COMMENT 'default - notes from reservation',
  `id_worker` int(11) DEFAULT NULL COMMENT 'not foreign key',
  `type` smallint(5) DEFAULT NULL COMMENT 'Type of cleaning (start, finish, intermediate)',
  `is_cancel` tinyint(1) DEFAULT NULL COMMENT 'default false: if reservation is_cancel=false ',
  `room_num` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`cleaning_id`),
  KEY `FK_regular_cleanings` (`id_reservation`),
  CONSTRAINT `FK_regular_cleanings` FOREIGN KEY (`id_reservation`) REFERENCES `reservation` (`id_reservation`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `regular_cleanings` */

/*Table structure for table `reservation` */

DROP TABLE IF EXISTS `reservation`;

CREATE TABLE `reservation` (
  `id_reservation` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) NOT NULL,
  `lock_start` date NOT NULL,
  `lock_finish` date NOT NULL,
  `notes` text COMMENT 'General notes - default for regular_cleanings',
  `is_cancel` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'If true - delete all records from regular_cleanings',
  `cleaning_days` varchar(10) DEFAULT NULL COMMENT 'Redo shedule',
  `created_time` timestamp NULL DEFAULT NULL,
  `id_manager` int(11) DEFAULT NULL,
  `group_t` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_reservation`),
  KEY `FK_reservation_rooms` (`id_room`),
  CONSTRAINT `FK_reservation_rooms` FOREIGN KEY (`id_room`) REFERENCES `rooms` (`id_room`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

/*Data for the table `reservation` */

insert  into `reservation`(`id_reservation`,`id_room`,`lock_start`,`lock_finish`,`notes`,`is_cancel`,`cleaning_days`,`created_time`,`id_manager`,`group_t`) values (48,5,'2015-03-17','2015-04-04','esfsef',0,NULL,NULL,NULL,''),(49,5,'2015-03-17','2015-04-04','esfsef',0,NULL,NULL,NULL,''),(50,2,'2015-03-03','2015-03-19','xdv',0,NULL,NULL,NULL,''),(51,2,'2015-03-03','2015-03-19','xdv',0,NULL,NULL,NULL,'');

/*Table structure for table `rooms` */

DROP TABLE IF EXISTS `rooms`;

CREATE TABLE `rooms` (
  `id_room` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(255) NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id_room`)
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=utf8;

/*Data for the table `rooms` */

insert  into `rooms`(`id_room`,`number`,`status`) values (1,'1',1),(2,'2',1),(3,'3',1),(4,'4',1),(5,'5',1),(6,'6',1),(7,'7',1),(8,'8',1),(9,'9',1),(10,'10',1),(11,'11',1),(12,'12',1),(13,'13',1),(14,'14',1),(15,'15',1),(16,'16',1),(17,'17',1),(18,'18',1),(19,'19',1),(20,'20',1),(21,'21',1),(22,'22',1),(23,'23',1),(24,'24',1),(25,'25',1),(26,'26',1),(27,'27',1),(28,'28',0),(29,'29',1),(30,'30',1),(31,'31',0),(32,'32',1),(33,'33',1),(34,'34',1),(35,'35',1),(36,'36',1),(37,'37',1),(38,'38',1),(39,'39',1),(40,'40',1),(41,'41',1),(42,'42',1),(43,'43',1),(44,'44',0),(45,'45',1),(46,'46',1),(47,'47',1),(48,'48',1),(49,'49',1),(50,'50',1),(51,'51',1),(52,'52',0),(53,'53',1),(54,'54',1),(55,'55',1),(56,'56',1),(57,'57',0),(58,'58',1),(59,'59',1),(60,'60',0),(61,'61',1),(62,'62',1),(63,'63',1),(64,'64',1),(65,'65',1),(66,'66',1),(67,'67',1),(68,'68',1),(69,'69',0),(70,'70',1),(71,'71',1),(72,'72',1),(73,'73',1),(74,'74',1),(75,'75',1),(76,'76',1),(77,'77',1),(78,'78',1),(79,'79',1),(80,'80',1),(81,'81',1),(82,'82',1),(83,'83',1),(84,'84',1),(85,'85',1),(86,'86',1),(87,'87',1),(88,'88',1),(89,'89',1),(90,'90',1),(91,'91',1),(92,'92',1),(93,'93',1),(94,'94',1),(95,'95',1),(96,'96',1),(97,'97',1),(98,'98',1),(99,'99',1),(100,'100',1),(101,'101',1),(102,'102',1),(103,'103',1),(104,'104',1),(105,'105',1),(106,'106',1),(107,'107',1),(108,'108',1),(109,'109',1),(110,'110',1),(111,'111',1),(112,'112',1),(113,'113',1),(114,'114',1),(115,'115',1),(116,'116',1),(117,'117',1),(118,'118',1),(119,'119',1),(120,'120',1),(121,'121',1),(122,'122',1),(123,'123',1),(124,'124',1),(125,'125',1),(126,'126',1),(127,'127',1),(128,'128',1),(129,'129',1),(130,'130',1),(131,'131',1),(132,'132',1),(133,'133',1),(134,'134',1),(135,'135',1),(136,'136',1),(137,'137',1),(138,'138',1),(139,'139',1),(140,'140',1),(141,'141',1),(142,'142',1),(143,'143',1),(144,'144',1),(145,'145',1),(146,'146',1),(147,'147',1),(148,'148',1),(149,'149',1),(150,'150',1),(151,'151',1),(152,'152',1),(153,'153',1),(154,'154',1),(155,'155',1),(156,'156',1),(157,'157',1),(158,'158',1),(159,'159',1),(160,'160',1),(161,'161',1),(162,'162',1),(163,'163',1),(164,'164',1),(165,'165',1),(166,'166',1),(167,'167',1),(168,'168',1),(169,'169',1),(170,'170',1),(171,'171',1),(172,'172',1),(173,'173',1),(174,'174',1),(175,'175',1),(176,'176',1),(177,'177',1),(178,'178',1),(179,'179',1),(180,'180',1),(181,'181',1),(182,'182',1),(183,'183',1),(184,'184',1),(185,'185',1),(186,'186',1),(187,'187',1),(188,'188',1),(189,'189',1),(190,'190',1),(191,'191',1),(192,'192',1),(193,'193',1),(194,'194',1),(195,'195',1),(196,'196',1),(197,'197',1),(198,'198',1),(199,'199',1),(200,'200',1),(201,'201',1),(202,'202',1),(203,'203',1),(204,'204',1),(205,'205',1),(206,'206',1),(207,'207',1),(208,'208',1),(209,'209',1),(210,'210',1),(211,'211',1),(212,'212',1),(213,'213',1),(214,'214',1),(215,'215',1),(216,'216',1),(217,'217',1),(218,'218',1),(219,'219',1);

/*Table structure for table `workers` */

DROP TABLE IF EXISTS `workers`;

CREATE TABLE `workers` (
  `id_worker` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_worker`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `workers` */

insert  into `workers`(`id_worker`,`name`,`phone`) values (1,'Natalia B','0502345689'),(2,'Andrii F','0345678432');

/* Trigger structure for table `workers` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `test` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `test` AFTER DELETE ON `workers` FOR EACH ROW BEGIN
    END */$$


DELIMITER ;

/* Procedure structure for procedure `test` */

/*!50003 DROP PROCEDURE IF EXISTS  `test` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `test`()
BEGIN
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
